package pl.unityt.recruitment.infrastructure;

import com.fasterxml.jackson.databind.ObjectMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import pl.unityt.recruitment.domain.Repositories;
import pl.unityt.recruitment.domain.errors.RepositoriesErrors;
import pl.unityt.recruitment.domain.errors.RepositoriesException;
import pl.unityt.recruitment.infrastructure.port.RepositoriesClient;

@Service
class RepositoriesClientImpl implements RepositoriesClient {
    private static final Logger logger = LoggerFactory.getLogger(RepositoriesClientImpl.class);

    private final HttpCustomClient httpCustomClient;
    private final String addressUrl;
    private final ObjectMapper objectMapper;

    public RepositoriesClientImpl(@Value("${app.address.api.github}") String addressUrl) {
        this.httpCustomClient = new HttpCustomClient();
        this.addressUrl = addressUrl;
        this.objectMapper = new ObjectMapper();
    }

    public Repositories getRepoInfo(String owner, String repoName) {
        try {
            String uri = CreatedURI.createUri(addressUrl, owner, repoName);

            String repoJSON = httpCustomClient.getRepoJSON(uri);

            return objectMapper.readValue(repoJSON, Repositories.class);
        } catch (Exception e) {
            logger.error(e.getMessage(), e);
            throw new RepositoriesException(RepositoriesErrors.INTERNAL_ERROR, e.getMessage());
        }
    }
}
