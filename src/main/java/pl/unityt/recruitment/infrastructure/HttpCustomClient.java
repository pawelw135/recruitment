package pl.unityt.recruitment.infrastructure;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import pl.unityt.recruitment.domain.errors.RepositoriesErrors;
import pl.unityt.recruitment.domain.errors.RepositoriesException;

import java.net.URI;
import java.net.http.HttpRequest;
import java.net.http.HttpResponse;
import java.util.concurrent.ExecutionException;

class HttpCustomClient {
    private static final Logger logger = LoggerFactory.getLogger(HttpCustomClient.class);

    private java.net.http.HttpClient httpClient;

    public HttpCustomClient() {
        this.httpClient = java.net.http.HttpClient.newHttpClient();
    }

    public String getRepoJSON(String address) {
        try {
            HttpRequest request = HttpRequest.newBuilder(URI.create(address))
                    .header("Accept", "application/json")
                    .build();

            return httpClient
                    .sendAsync(request, HttpResponse.BodyHandlers.ofString())
                    .thenApply(HttpResponse::body)
                    .get();

        } catch (InterruptedException | IllegalArgumentException | ExecutionException e) {
            logger.error(e.getMessage(), e);
            throw new RepositoriesException(RepositoriesErrors.INTERNAL_ERROR, e.getMessage());
        }
    }
}
