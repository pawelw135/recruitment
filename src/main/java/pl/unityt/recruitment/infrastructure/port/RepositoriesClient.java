package pl.unityt.recruitment.infrastructure.port;

import pl.unityt.recruitment.domain.Repositories;

public interface RepositoriesClient {
    Repositories getRepoInfo(String owner, String repoName);
}
