package pl.unityt.recruitment.infrastructure;


class CreatedURI {
    public static String createUri(String addressUrl, String owner, String repoName) {
        return String.format("%s%s/%s", addressUrl, owner, repoName);
    }
}
