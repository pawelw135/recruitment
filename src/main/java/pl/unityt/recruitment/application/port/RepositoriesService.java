package pl.unityt.recruitment.application.port;

import pl.unityt.recruitment.domain.Repositories;

public interface RepositoriesService {
    Repositories getRepoInfo(String owner, String repoName);
}
