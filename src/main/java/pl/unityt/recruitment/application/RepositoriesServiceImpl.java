package pl.unityt.recruitment.application;

import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;
import pl.unityt.recruitment.application.port.RepositoriesService;
import pl.unityt.recruitment.domain.errors.RepositoriesErrors;
import pl.unityt.recruitment.domain.errors.RepositoriesException;
import pl.unityt.recruitment.domain.Repositories;
import pl.unityt.recruitment.infrastructure.port.RepositoriesClient;

import java.util.stream.Stream;

@Service
class RepositoriesServiceImpl implements RepositoriesService {

    private final RepositoriesClient repositoriesClientImpl;

    public RepositoriesServiceImpl(RepositoriesClient repositoriesClientImpl) {
        this.repositoriesClientImpl = repositoriesClientImpl;
    }

    @Override
    public Repositories getRepoInfo(String owner, String repoName) {
        if (StringUtils.isEmpty(owner.trim()) || StringUtils.isEmpty(repoName.trim()))
            throw new RepositoriesException(RepositoriesErrors.WRONG_DATA, "Empty or wrong parameters.");

        Repositories repositories = repositoriesClientImpl.getRepoInfo(owner, repoName);

        checkValid(repositories);

        return repositories;
    }

    private void checkValid(Repositories repositories) {
        if (repositories == null)
            throw new RepositoriesException(RepositoriesErrors.NOT_FOUND, "Not found repository.");

        boolean check = Stream.of(repositories.getFullName(), repositories.getDescription(), repositories.getCloneUrl(), repositories.getStar(), repositories.getCreatedAt())
                .allMatch(StringUtils::isEmpty);
        if (check)
            throw new RepositoriesException(RepositoriesErrors.NOT_FOUND, "Not found repository.");
    }
}
