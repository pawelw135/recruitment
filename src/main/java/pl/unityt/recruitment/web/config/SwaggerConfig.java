package pl.unityt.recruitment.web.config;

import io.swagger.v3.oas.models.OpenAPI;
import io.swagger.v3.oas.models.info.Contact;
import io.swagger.v3.oas.models.info.Info;
import io.swagger.v3.oas.models.servers.Server;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class SwaggerConfig {

    private final String swaggerDocumentUrl;

    public SwaggerConfig(@Value("${swagger.document.url}") String swaggerDocumentUrl) {
        this.swaggerDocumentUrl = swaggerDocumentUrl;
    }

    @Bean
    public OpenAPI customOpenAPI() {
        Contact contact = new Contact();
        contact.setName("Paweł Walczuk");
        contact.setEmail("walczuk135@gmail.com");

        Server server = new Server();
        server.setUrl(swaggerDocumentUrl);

        return new OpenAPI()
                .info(new Info()
                        .title("Recruitment task")
                        .version("v1")
                        .description("Recruitment task: Github repository api info"))
                .addServersItem(server);
    }
}
