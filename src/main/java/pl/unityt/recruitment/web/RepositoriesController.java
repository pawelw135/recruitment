package pl.unityt.recruitment.web;

import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.media.Content;
import io.swagger.v3.oas.annotations.media.ExampleObject;
import io.swagger.v3.oas.annotations.media.Schema;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import io.swagger.v3.oas.annotations.responses.ApiResponses;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import pl.unityt.recruitment.application.port.RepositoriesService;
import pl.unityt.recruitment.domain.Repositories;
import pl.unityt.recruitment.web.errors.RestApiException;
import pl.unityt.recruitment.web.example.ExampleResponses;

@RestController
@RequestMapping("/repositories")
public class RepositoriesController {

    private final RepositoriesService repositoriesService;

    public RepositoriesController(RepositoriesService repositoriesService) {
        this.repositoriesService = repositoriesService;
    }

    @Operation(summary = "/repositories/{owner}/{repositoryName}", method = "GET")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200", content = {@Content(mediaType = "application/json",
                    schema = @Schema(implementation = Repositories.class),
                    examples = {@ExampleObject(value = ExampleResponses.REPOSITORY_OK)})}),
            @ApiResponse(responseCode = "400", content = {@Content(mediaType = "application/json",
                    schema = @Schema(implementation = RestApiException.class),
                    examples = {@ExampleObject(value = ExampleResponses.REPOSITORY_WRONG_DATA)})}),
            @ApiResponse(responseCode = "404", content = {@Content(mediaType = "application/json",
                    schema = @Schema(implementation = RestApiException.class),
                    examples = {@ExampleObject(value = ExampleResponses.REPOSITORY_NOT_FOUND)})}),
            @ApiResponse(responseCode = "500", content = {@Content(mediaType = "application/json",
                    schema = @Schema(implementation = RestApiException.class),
                    examples = {@ExampleObject(value = ExampleResponses.REPOSITORY_INTERNAL_ERROR)})})
    })
    @GetMapping("/{owner}/{repositoryName}")
    public ResponseEntity<?> repositories(@PathVariable String owner, @PathVariable String repositoryName) {
        Repositories repositories = repositoriesService.getRepoInfo(owner, repositoryName);

        return new ResponseEntity<>(repositories, HttpStatus.OK);
    }
}
