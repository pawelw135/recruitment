package pl.unityt.recruitment.web.example;

public class ExampleResponses {
    public static final String REPOSITORY_OK = "{\n" +
            "    \"fullName\": \"walczuk135/Car-Service\",\n" +
            "    \"description\": \"Service with car offers.\",\n" +
            "    \"cloneUrl\": \"https://github.com/walczuk135/Car-Service.git\",\n" +
            "    \"star\": 0,\n" +
            "    \"createdAt\": \"2018-12-12T00:13:09\"\n" +
            "}";

    public static final String REPOSITORY_NOT_FOUND = "{\n" +
            "    \"timestamp\": \"2021-05-20T00:27:01\",\n" +
            "    \"type\": \"about:blank\",\n" +
            "    \"title\": \"Not found repository.\",\n" +
            "    \"code\": \"NOT_FOUND\",\n" +
            "    \"status\": 404\n" +
            "}";

    public static final String REPOSITORY_INTERNAL_ERROR = "{\n" +
            "    \"timestamp\": \"2021-05-20T20:41:18\",\n" +
            "    \"type\": \"about:blank\",\n" +
            "    \"title\": \"java.net.ConnectException: No route to host: no further information\",\n" +
            "    \"code\": \"INTERNAL_ERROR\",\n" +
            "    \"status\": 500\n" +
            "}";

    public static final String REPOSITORY_WRONG_DATA = "{\n" +
            "    \"timestamp\": \"2021-05-20T20:27:59\",\n" +
            "    \"type\": \"about:blank\",\n" +
            "    \"title\": \"Empty or wrong parameters.\",\n" +
            "    \"code\": \"WRONG_DATA\",\n" +
            "    \"status\": 400\n" +
            "}";
}
