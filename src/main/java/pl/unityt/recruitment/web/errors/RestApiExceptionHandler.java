package pl.unityt.recruitment.web.errors;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.RestControllerAdvice;
import pl.unityt.recruitment.domain.errors.RepositoriesErrors;
import pl.unityt.recruitment.domain.errors.RepositoriesException;

import java.time.LocalDateTime;

@RestControllerAdvice
public class RestApiExceptionHandler {
    private static final Logger logger = LoggerFactory.getLogger(RestApiExceptionHandler.class);

    @ExceptionHandler(value = {RepositoriesException.class})
    public ResponseEntity<?> handleApiException(RepositoriesException e) {
        RestApiException restApiException = null;
        HttpStatus httpStatus = null;
        if (e.getCode().equals(RepositoriesErrors.NOT_FOUND)) {
            restApiException = notFountException(e);
            httpStatus = HttpStatus.NOT_FOUND;
        }
        if (e.getCode().equals(RepositoriesErrors.WRONG_DATA)) {
            restApiException = badRequestException(e);
            httpStatus = HttpStatus.BAD_REQUEST;
        }
        if (e.getCode().equals(RepositoriesErrors.INTERNAL_ERROR)) {
            restApiException = internalServerError(e);
            httpStatus = HttpStatus.INTERNAL_SERVER_ERROR;
        }

        logger.error(e.getMessage(), e);
        return new ResponseEntity<>(restApiException, httpStatus);
    }

    private RestApiException notFountException(Exception e) {
        return new RestApiException(LocalDateTime.now(), "about:blank", e.getMessage(), RepositoriesErrors.NOT_FOUND, HttpStatus.NOT_FOUND.value());
    }

    private RestApiException badRequestException(Exception e) {
        return new RestApiException(LocalDateTime.now(), "about:blank", e.getMessage(), RepositoriesErrors.WRONG_DATA, HttpStatus.BAD_REQUEST.value());
    }

    private RestApiException internalServerError(Exception e) {
        return new RestApiException(LocalDateTime.now(), "about:blank", e.getMessage(), RepositoriesErrors.INTERNAL_ERROR, HttpStatus.INTERNAL_SERVER_ERROR.value());
    }
}
