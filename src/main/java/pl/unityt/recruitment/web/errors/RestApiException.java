package pl.unityt.recruitment.web.errors;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.fasterxml.jackson.datatype.jsr310.ser.LocalDateTimeSerializer;

import java.time.LocalDateTime;

public class RestApiException {
    @JsonSerialize(using = LocalDateTimeSerializer.class)
    @JsonFormat(pattern = "yyyy-MM-dd'T'HH:mm:ss")
    private LocalDateTime timestamp = LocalDateTime.now();
    private String type;
    private String title;
    private String code;
    private Integer status;

    public RestApiException(LocalDateTime timestamp, String type, String title, String code, Integer status) {
        this.timestamp = timestamp;
        this.type = type;
        this.title = title;
        this.code = code;
        this.status = status;
    }

    public RestApiException() {
    }

    public LocalDateTime getTimestamp() {
        return timestamp;
    }

    public String getType() {
        return type;
    }

    public String getTitle() {
        return title;
    }

    public String getCode() {
        return code;
    }

    public Integer getStatus() {
        return status;
    }
}
