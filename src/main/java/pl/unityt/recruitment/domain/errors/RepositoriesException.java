package pl.unityt.recruitment.domain.errors;

public class RepositoriesException extends RuntimeException {
    private final String code;

    public RepositoriesException(String code, String message) {
        super(message);
        this.code = code;
    }

    public String getCode() {
        return code;
    }
}
