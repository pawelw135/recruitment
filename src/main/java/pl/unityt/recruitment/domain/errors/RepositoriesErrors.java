package pl.unityt.recruitment.domain.errors;

public class RepositoriesErrors {
    public static final String NOT_FOUND = "NOT_FOUND";
    public static final String WRONG_DATA = "WRONG_DATA";
    public static final String INTERNAL_ERROR = "INTERNAL_ERROR";
}
