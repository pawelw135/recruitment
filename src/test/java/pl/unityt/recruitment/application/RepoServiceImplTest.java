package pl.unityt.recruitment.application;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.BDDMockito;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.boot.test.context.SpringBootTest;
import pl.unityt.recruitment.application.port.RepositoriesService;
import pl.unityt.recruitment.domain.errors.RepositoriesErrors;
import pl.unityt.recruitment.domain.errors.RepositoriesException;
import pl.unityt.recruitment.domain.Repositories;


import java.time.LocalDateTime;

import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.mockito.BDDMockito.willThrow;
import static org.mockito.Mockito.when;

@ExtendWith(MockitoExtension.class)
@SpringBootTest
class RepoServiceImplTest {

    @Mock
    private RepositoriesService repoService;

    @Test
    public void testGetJsonStringSuccess() {
        //given
        Repositories repoInfo = new Repositories("walczuk135/Car-Service", "Service with car offers.", "https://github.com/walczuk135/Car-Service.git", 0, LocalDateTime.parse("2018-12-12T00:13:09"));
        //when
        when(repoService.getRepoInfo("walczuk135", "Car-Service")).thenReturn(repoInfo);
        Repositories returnedResult = repoService.getRepoInfo("walczuk135", "Car-Service");
        //then
        assertThat(returnedResult).isEqualTo(repoInfo);
    }

    @Test
    public void testRepositoryNotFound() {
        //given
        String exceptionMessage = "Not found repository.";
        String exceptionCode = RepositoriesErrors.NOT_FOUND;
        //when
        BDDMockito.willThrow(new RepositoriesException(RepositoriesErrors.NOT_FOUND, "Not found repository.")).given(repoService).getRepoInfo("walczuk135", "Car-Servicek");
        RepositoriesException exception = assertThrows(
                RepositoriesException.class,
                () -> repoService.getRepoInfo("walczuk135", "Car-Servicek"));
        //then
        assertThat(exceptionMessage).isEqualTo(exception.getMessage());
        assertThat(exceptionCode).isEqualTo(exception.getCode());
    }

    @Test
    public void testRepositoryBadParameters() {
        //given
        String exceptionMessage = "Empty or wrong parameters.";
        String exceptionCode = RepositoriesErrors.WRONG_DATA;
        //when
        willThrow(new RepositoriesException(RepositoriesErrors.WRONG_DATA, "Empty or wrong parameters.")).given(repoService).getRepoInfo("walczuk135", " ");
        RepositoriesException exception = assertThrows(
                RepositoriesException.class,
                () -> repoService.getRepoInfo("walczuk135", " "));
        //then
        assertThat(exceptionMessage).isEqualTo(exception.getMessage());
        assertThat(exceptionCode).isEqualTo(exception.getCode());
    }

}