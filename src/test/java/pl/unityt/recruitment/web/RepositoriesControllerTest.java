package pl.unityt.recruitment.web;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.ResultActions;
import pl.unityt.recruitment.application.port.RepositoriesService;
import pl.unityt.recruitment.domain.errors.RepositoriesErrors;
import pl.unityt.recruitment.domain.Repositories;
import pl.unityt.recruitment.domain.errors.RepositoriesException;


import java.time.LocalDateTime;

import static org.mockito.BDDMockito.given;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;


@ExtendWith(SpringExtension.class)
@SpringBootTest
@AutoConfigureMockMvc
class RepositoriesControllerTest {

    @Autowired
    private MockMvc mockMvc;

    @MockBean
    private RepositoriesService repoService;

    @Test
    public void testGetJobOfferAndDetailsIdFound() throws Exception {
        //given
        Repositories repoInfo = new Repositories("walczuk135/Car-Service", "Service with car offers.", "https://github.com/walczuk135/Car-Service.git", 0, LocalDateTime.parse("2018-12-12T00:13:09"));
        given(repoService.getRepoInfo("walczuk135", "Car-Service")).willReturn(repoInfo);
        //when
        ResultActions result = mockMvc.perform(get("/repositories/walczuk135/Car-Service"));
        //then
        result.andExpect(content().contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.fullName").value("walczuk135/Car-Service"))
                .andExpect(jsonPath("$.description").value("Service with car offers."))
                .andExpect(jsonPath("$.cloneUrl").value("https://github.com/walczuk135/Car-Service.git"))
                .andExpect(jsonPath("$.star").value(0))
                .andExpect(jsonPath("$.createdAt").value("2018-12-12T00:13:09"));
    }

    @Test
    public void testGetJobOfferAndDetailsIdNotFound() throws Exception {
        //given
        given(repoService.getRepoInfo("walczuk135", "Car-Servicek")).willThrow(new RepositoriesException(RepositoriesErrors.NOT_FOUND, "Not found repository."));
        //when
        ResultActions result = mockMvc.perform(get("/repositories/walczuk135/Car-Servicek"));
        //then
        result.andExpect(content().contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isNotFound());
    }

    @Test
    public void testGetJobOfferAndDetailsIdWrongParameters() throws Exception {
        //given
        given(repoService.getRepoInfo(" ", " ")).willThrow(new RepositoriesException(RepositoriesErrors.WRONG_DATA, "Empty or wrong parameters."));
        //when
        ResultActions result = mockMvc.perform(get("/repositories/ / "));
        //then
        result.andExpect(content().contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isBadRequest());
    }

}